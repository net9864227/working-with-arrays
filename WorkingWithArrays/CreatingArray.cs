﻿using System;

namespace WorkingWithArrays
{
    public static class CreatingArray
    {
        public static int[] CreateEmptyArrayOfIntegers()
        {
            int[] arr = Array.Empty<int>();
            return arr;
        }

        public static bool[] CreateEmptyArrayOfBooleans()
        {
            bool[] arr = Array.Empty<bool>();
            return arr;
        }

        public static string[] CreateEmptyArrayOfStrings()
        {
            string[] arr = Array.Empty<string>();
            return arr;
        }

        public static char[] CreateEmptyArrayOfCharacters()
        {
            char[] arr = Array.Empty<char>();
            return arr;
        }

        public static double[] CreateEmptyArrayOfDoubles()
        {
            double[] arr = Array.Empty<double>();
            return arr;
        }

        public static float[] CreateEmptyArrayOfFloats()
        {
            float[] arr = Array.Empty<float>();
            return arr;
        }

        public static decimal[] CreateEmptyArrayOfDecimals()
        {
            decimal[] arr = Array.Empty<decimal>();
            return arr;
        }

        public static int[] CreateArrayOfTenIntegersWithDefaultValues()
        {
            int[] arr = new int[10];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = 0;
            }

            return arr;
        }

        public static bool[] CreateArrayOfTwentyBooleansWithDefaultValues()
        {
            bool[] arr = new bool[20];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = false;
            }

            return arr;
        }

        public static string[] CreateArrayOfFiveEmptyStrings()
        {
            string[] arr = new string[5];
            return arr;
        }

        public static char[] CreateArrayOfFifteenCharactersWithDefaultValues()
        {
            char[] arr = new char[15];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = default(char);
            }

            return arr;
        }

        public static double[] CreateArrayOfEighteenDoublesWithDefaultValues()
        {
            double[] arr = new double[18];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = default(double);
            }

            return arr;
        }

        public static float[] CreateArrayOfOneHundredFloatsWithDefaultValues()
        {
            float[] arr = new float[100];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = default(float);
            }

            return arr;
        }

        public static decimal[] CreateArrayOfOneThousandDecimalsWithDefaultValues()
        {
            decimal[] arr = new decimal[1000];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = default(decimal);
            }

            return arr;
        }

        public static int[] CreateIntArrayWithOneElement()
        {
            int[] arr = new int[] { 123456 };
            return arr;
        }

        public static int[] CreateIntArrayWithTwoElements()
        {
            int[] arr = new int[] { 1111111, 9999999 };
            return arr;
        }

        public static int[] CreateIntArrayWithTenElements()
        {
            int[] arr = new int[] { 0, 4234, 3845, 2942, 1104, 9794, 923943, 7537, 4162, 10134 };
            return arr;
        }

        public static bool[] CreateBoolArrayWithOneElement()
        {
            bool[] arr = new bool[] { true };
            return arr;
        }

        public static bool[] CreateBoolArrayWithFiveElements()
        {
            bool[] arr = new bool[] { true, false, true, false, true };
            return arr;
        }

        public static bool[] CreateBoolArrayWithSevenElements()
        {
            bool[] arr = new bool[] { false, true, true, false, true, true,  false };
            return arr;
        }

        public static string[] CreateStringArrayWithOneElement()
        {
            string[] arr = new string[] { "one" };
            return arr;
        }

        public static string[] CreateStringArrayWithThreeElements()
        {
            string[] arr = new string[] { "one", "two", "three" };
            return arr;
        }

        public static string[] CreateStringArrayWithSixElements()
        {
            string[] arr = new string[] { "one", "two", "three", "four", "five", "six" };
            return arr;
        }

        public static char[] CreateCharArrayWithOneElement()
        {
            char[] arr = new char[] { 'a' };
            return arr;
        }

        public static char[] CreateCharArrayWithThreeElements()
        {
            char[] arr = new char[] { 'a', 'b', 'c' };
            return arr;
        }

        public static char[] CreateCharArrayWithNineElements()
        {
            char[] arr = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'a' };
            return arr;
        }

        public static double[] CreateDoubleArrayWithOneElement()
        {
            double[] arr = new double[] { 1.12 };
            return arr;
        }

        public static double[] CreateDoubleWithFiveElements()
        {
            double[] arr = new double[] { 1.12, 2.23, 3.34, 4.45, 5.56 };
            return arr;
        }

        public static double[] CreateDoubleWithNineElements()
        {
            double[] arr = new double[] { 1.12, 2.23, 3.34, 4.45, 5.56, 6.67, 7.78, 8.89, 9.91 };
            return arr;
        }

        public static float[] CreateFloatArrayWithOneElement()
        {
             float[] arr = new float[] { 123456789.123456F };
             return arr;
        }

        public static float[] CreateFloatWithThreeElements()
        {
            float[] arr = new float[] { 1000000.123456F, 2223334444.123456F, 9999.999999F };
            return arr;
        }

        public static float[] CreateFloatWithFiveElements()
        {
            float[] arr = new float[] { 1.0123F, 20.012345F, 300.01234567F, 4000.01234567F, 500000.01234567F };
            return arr;
        }

        public static decimal[] CreateDecimalArrayWithOneElement()
        {
            decimal[] arr = new decimal[] { 10000.123456M };
            return arr;
        }

        public static decimal[] CreateDecimalWithFiveElements()
        {
            decimal[] arr = new decimal[] { 1000.1234M, 100000.2345M, 100000.3456M, 1000000.456789M, 10000000.5678901M };
            return arr;
        }

        public static decimal[] CreateDecimalWithNineElements()
        {
            decimal[] arr = new decimal[] { 10.122112M, 200.233223M, 3000.344334M, 40000.455445M, 500000.566556M, 6000000.677667M, 70000000.788778M, 800000000.899889M, 9000000000.911991M };
            return arr;
        }
    }
}
